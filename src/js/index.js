$(function(){
    console.log("hola mundo");
    productlist.init();
});

let productlist = {
    init: function(){
        $.when(productlist.renderProducts()).done(function () {
            $.when(productlist.formatPrice()).done(function () {
                productlist.calculate();
            });
        });
    },
    productTemplate: (img, name, detail, price) => {
        return '<div class="productBox__container">'+'<div class="productBox__content">'+
        '<div class="productBox__img">'+img+'</div>'+'<div class="productBox__name">'+name+'</div>'+
        '<div class="productBox__detail">'+detail+'</div>'+'<div class="productBox__price">'+
        price +'</div>'+'<div class="productBox__buyBtn">Comprar</div>'+'</div>'+'</div>';
    },
    renderProducts: () =>{
        let product = [{
            "name": "Product 1",
            "detail": "Lorem ipsum dolor sit amet",
            "price": "25000",
            "info": "This is the latest and greatest product from Derp corp.",
            "image": "http://placehold.it/300x300/2ecc71/fffffff"
        }, {
            "name": "Product 2",
            "detail": "Lorem ipsum dolor sit amet",
            "price": "1200",
            "offer": "BOGOF",
            "image": "http://placehold.it/300x300/8e44ad/fffffff"
        }, {
            "name": "Product 3",
            "detail": "Lorem ipsum dolor sit amet",
            "price": "10000",
            "image": "http://placehold.it/300x300/34495e/fffffff"
        }, {
            "name": "Product 4",
            "detail": "Lorem ipsum dolor sit amet",
            "price": "3000",
            "offer": "No srsly GTFO",
            "image": "http://placehold.it/300x300/e67e22/fffffff"
        }, {
            "name": "Product 5",
            "detail": "Lorem ipsum dolor sit amet",
            "price": "4500",
            "offer": "No srsly GTFO",
            "image": "http://placehold.it/300x300/16a085/fffffff"
        }];
        // let allproducts = product.map(data => data.price + data.detail);
        // let allproducts = product.map(data => productList.productTemplate(data.image,data.name, data.detail, data.price));
        let allproducts = product.map(data => `<div class="productBox__container"><div class="productBox__content">
        <div class="productBox__img"><img src="${data.image}" alt="product-img"></div><div class="productBox__name">${data.name}</div>
        <div class="productBox__detail">${data.detail}</div><div class="productBox__price">
        ${data.price}</div><a class="productBox__remove" onClick="removeProduct()">Quitar del carro</a><a class="productBox__buyBtn">Comprar</a></div></div>`);
        
        $("#productList").append(allproducts);
    },
    formatPrice: function(){
        let $prat = $(".productBox__content");
        $prat.each(function(){
            let _this = $(this);
            let _thisPrice = parseInt(_this.find(".productBox__price").text());
            _this.find(".productBox__price").text(_thisPrice.toLocaleString());
        });
    },
    calculate: function(){
        let $buyBtn = $(".productBox__buyBtn");
        let $cartotal = parseInt($("#cartTotal").text());
        $buyBtn.on('click', function(e){
            e.preventDefault();
            let _this = $(this);
            let _thisPrice = parseInt(_this.parent().find(".productBox__price").text().replace(".",""));
            let _thisProductContent = _this.parent().clone().addClass("added");
            $("#cartTotal").text(($cartotal += _thisPrice).toLocaleString());
            $("#miniCartProducts").append(_thisProductContent);
        });
    }
};

function removeProduct() {
    let $removeBtn = $(".productBox__remove");
    let $cartotal = parseInt($("#cartTotal").text().replace(".", ""));
    $removeBtn.on("click", function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        let _this = $(this);
        let _thisPrice = parseInt(_this.parent().find(".productBox__price").text().replace(".", ""));
        console.log(_thisPrice);
        $("#cartTotal").text(($cartotal - _thisPrice).toLocaleString());
        _this.parent().remove();
    });
}